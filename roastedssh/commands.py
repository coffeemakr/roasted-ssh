'''
Created on Mar 25, 2014

@author: c0ff3m4kr
'''

class Commands(object):

    def __init__(self, transport):
        self.transport = transport
        
    def run(self, command):
        if not isinstance(command, list):
            raise TypeError('command has to be a list')
        if hasattr(self, command[0]):
            getattr(self, command[0])(command[1:])
        else:
            sys.stdout.write("ERROR: command %r not found\n" % command)
            sys.stdout.flush()




    def show(self, args):
        """
        
        """
        what = args[0]
        _print();
        if what == 'username':
            what = self.transport.get_username()
        elif what == 'banner':
            what = self.transport.get_banner()
        elif what == "key":
            if len(args) > 0 and args[0] == 'private':
                key = self.transport.get_server_key()
            else:
                key = self.transport.get_remote_server_key()
            
            what = key.__class__.__name__
            what += '\r\nfingerprint: '+get_fingerprint_string(key.get_fingerprint())
        elif what == 'peer':
            what = self.transport.getpeername()
            what = what[0] + str(what[1])
            
        else:
            _print("invalid value. choose one of these: username, banner, key, peer")
        _print(what)
        
    def clean(self, *args):
        """
        Clean command history.
        """
        cmds = ['rm ~/.bash_history',
                'history -c']
        return cmds
        
    def quit(self, *args):
        """
        Quit the shell without proper execution of .bash_logout or anything
        else.
        """
        _print("goodbye!" + Style.RESET_ALL)
        self.transport.close()
        sys.exit()
        