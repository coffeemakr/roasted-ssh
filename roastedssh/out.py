'''
Created on Mar 25, 2014

@author: c0ff3m4kr
'''

from colorama import init, Fore, Back, Style
init()

def warning(message):
    print(Style.BRIGHT+Fore.YELLOW + 'WARNING: '+ Style.RESET_ALL + message )
    
def error(message):
    print(Style.BRIGHT+Fore.RED + ' :(      '+ Style.RESET_ALL + message )

def success(message):
    print(Fore.GREEN+message+Fore.RESET)
    
def get_fingerprint(key):
    s = [] 
    for c in key.get_fingerprint():
        s.append('%02x' % c)
    return ':'.join(s)