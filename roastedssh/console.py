'''
.. module:: roastedssh.console

This module provides the `RoastedConsole` class.

.. author:: c0ff3m4kr
'''
import socket
import sys
from paramiko.py3compat import u
import paramiko
from commands import Commands
import out
import traceback
import os
import getpass

# windows does not have termios...
try:
    import termios
    has_termios = True
except ImportError:
    has_termios = False

if has_termios:
    import tty
    import select
else:
    import threading
    
    
class ConsoleError(Exception):
    def __init__(self, *args):
        super(ConsoleError, self).__init__(*args)
        self.msg = args[0]
    
    def __str__(self):
        return self.msg
    
def _print(what=''):
    if not isinstance(what, bytes) and not isinstance(what, str):
        what = repr(what)
    if isinstance(what, bytes):
        what = what.decode('utf-8')
    sys.stdout.write(what+'\n\r')
    sys.stdout.flush()
   

class RoastedConsole(object):
    """
    Interactive SSH Console 
    """
    
    log = out
    
    knownhost_files = ['~/.ssh/known_hosts',
                       '~/ssh/known_hosts',
                       '/etc/ssh/ssh_known_hosts']
    
    def __init__(self, transport):
        # setup logging
        paramiko.util.log_to_file('console.log')

        if not isinstance(transport, paramiko.Transport):
            raise TypeError('transport has to be a instance of '
                            'paramiko.Transport')
        self.transport = transport
        self.commands = Commands(transport)
        self.channel = None                
        self.__username = None
        
    @property
    def username(self):
        """
        Readonly attribute.
        """
        return self.__username
    
    @username.setter
    def username(self, username):
        if not username:
            raise ValueError('username can not be empty')
        if isinstance(username, bytes):
            username = username.decode('utf-8')
        self.__username = username

    
    def _ask_user_input(self, what, hidden=False, default=None):
        if default:
            what += ' [%s]' % default
        query = '%-20s:' % what
        if hidden:
            res = getpass.getpass(query)
        else:
            res = input(query)
        if res == '' and default is None:
            res = default
        return res
    
    def _ask_user(self, what, default=None, accept_invalid=False):
        """
        Ask the user a yes or no question.
        
        If default is :const:`None` the user has to give a answer.
        
        :param default: If :const:`True` the answer defaults to `yes`
                        and on :const:`False` to `no`. 
        """
        default = ''
        query = 'y/n'
        if default is True:
            default = 'y'
            query = 'Y/n'
        elif default is False:
            default = 'n'
            query = 'y/N'
        query = what + '[%s]' % query
        answer = None
        while answer is None:
            a = input(query)
            if not a:
                a = default
            a = a.lower()
            if a in ('y', 'yes'):
                answer = True
            elif a in ('n', 'no'):
                answer = False
        return answer
        
    def hostkey_is_known(self, interactive=False):
        """
        Check if the remote key is in one of the :attr:`knownhost_files`.
        
        Returns :const:`True` if the hostkey is in a file and :const:`False` if not.
        """
        known_keys = {}
        for filename in self.knownhost_files:
            try:
                known_keys = paramiko.util.load_host_keys(os.path.expanduser(filename))
            except IOError:
                continue
            foreign_key = self.transport.get_remote_server_key()
            foreign_fingerprint = out.get_fingerprint(foreign_key)
            # check server's host key -- this is important.
            if hostname not in known_keys:
                self.log.warning('Unknown host key!\n%r' % foreign_fingerprint)
                
            elif foreign_key.get_name() not in known_keys[hostname]:
                self.log.warning('Unknown host key!\n%r' % foreign_fingerprint)
            elif known_keys[hostname][foreign_key.get_name()] != foreign_key:
                self.log.warning('Host key has changed!!!\n%r' % foreign_fingerprint)
            else:
                self.log.success('Host key is known.')
                return True
        return False
    
    @property
    def transport(self):
        return self.__transport
    
    @transport.setter
    def transport(self, value):
        self.__transport = value
        self.commands = Commands(value)
        value.start_client()
        
    @classmethod
    def from_nothing(cls, hostname, port=22):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.connect((hostname, port))
        except socket.error as e:
            cls.log.error(str(e))
            raise ConsoleError('Connect failed')
        except Exception as e:
            #: .. todo:: exception handling to log
            traceback.print_exc()
            raise ConsoleError('Connect failed')
        return cls.from_socket(sock)
        #transport = paramiko.Transport((hostname,port))
        #return cls(transport)
    
    @classmethod
    def from_socket(cls, sock):
        """
        Create console from a connected socket.
        
        .. note:: The socket has to be already connected.
        """
        if not isinstance(sock, socket.socket):
            raise TypeError('sock is no socket!')
        transport = paramiko.Transport(sock)
        return cls(transport)
    
    
    def login(self, username=None, password=None):
        """
        Log in interactivly
        """
        if username is None:
            username = getpass.getuser()
        
        if not username is None:
            self.username = username
        
        if self.username is None:
            self.username = self._ask_user_input('username')
        if self.username is None:
            raise RuntimeError('no username')
        if password is None:
            password = self._ask_user_input('password', hidden=True)
        try:
            next_auths = self.transport.auth_password(username, password)
        except paramiko.AuthenticationException as why:
            self.log.error(str(why))
            return False
        if next_auths:
            raise NotImplementedError(next_auths)
        if self.transport.is_authenticated():
            self.log.success("successfully logged in")
        return True
        
    @staticmethod
    def get_fingerprint_string(key, transport=None):
        s = ''
        for c in key.get_fingerprint():
            s += "%02x:" % c
        return s[:-1]  
        
    def __del__(self):
        self.close()
    
    def close(self):
        """
        
        """
        if self.channel:
            # close session
            self.channel.close()
            self.channel = None
            
    def start(self):
        # open new session
        self.channel = self.transport.open_session()
        self.channel.get_pty()
        self.channel.invoke_shell()
        print("** hello :D")
        if has_termios:
            self.__posix_shell()
        else:
            self.__window_shell()        
        
    def run_command(self, command):
        """
        Runs a command string.
        """
        command = command.split(' ')
        commands = self.commands.run(command)
        if not commands is None:
            for command in commands:
                self.channel.send(command+'\r')
                    
    def handle_input(self, x):
        """
        
        """
        if ord(x) == 24 and not self.command_enabled and self.command_chunk=='':
            sys.stdout.write(Fore.BLUE)
            sys.stdout.flush()
            self.command_enabled = True
            self.command_chunk = ''
            
        elif ord(x) == 3:
            # ctr+c
            self.command_enabled = False
            sys.stdout.write(Fore.RESET)
            sys.stdout.flush()
            self.command_chunk = ''
            self.channel.send(x)
        elif x in '\n\r':
            # newline 
            if self.command_enabled:
                sys.stdout.write(x + Fore.RESET)
                sys.stdout.flush()
                self.run_command(self.command_chunk)
            self.command_enabled = False
            self.command_chunk = ''
            self.channel.send(x)
        elif self.command_enabled and ord(x) == 127:
            # backspace
            if len(self.command_chunk) > 0:
                self.command_chunk = self.command_chunk[:len(self.command_chunk)-1]
                sys.stdout.write('\b \b')
                sys.stdout.flush()
        elif self.command_enabled:
            # other characters when command is enabled
            self.command_chunk += x
            sys.stdout.write(x)
            sys.stdout.flush()
        else:
            # nothing special -> just emit
            self.channel.send(x)
    
    def __posix_shell(self):
        oldtty = termios.tcgetattr(sys.stdin)
        try:
            tty.setraw(sys.stdin.fileno())
            tty.setcbreak(sys.stdin.fileno())
            self.channel.settimeout(0.0)
            self.command_chunk = ''
            self.command_enabled = False
            while True:
                r, w, e = select.select([self.channel, sys.stdin], [], [])
                if self.channel in r:
                    try:
                        x = self.channel.recv(1024).encode('utf-8')
                        if len(x) == 0:
                            sys.stdout.write('\r\n<< EOF >>\r\n')
                            break
                        sys.stdout.write(x)
                        sys.stdout.flush()
                    except socket.timeout:
                        pass
                if sys.stdin in r:
                    x = sys.stdin.read(1)
                    if len(x) == 0:
                        break
                    self.handle_input(x)
                    
        finally:
            termios.tcsetattr(sys.stdin, termios.TCSADRAIN, oldtty)
    
        
    # thanks to Mike Looijmans for this code
    def __window_shell(self):
        """
        
        """
        
    
        sys.stdout.write("Line-buffered terminal emulation. Press F6 or ^Z to send EOF.\r\n\r\n")
            
        def writeall(sock):
            while True:
                data = sock.recv(256)
                if not data:
                    sys.stdout.write('\r\n*** EOF ***\r\n\r\n')
                    sys.stdout.flush()
                    break
                sys.stdout.write(data)
                sys.stdout.flush()
            
        writer = threading.Thread(target=writeall, args=(self.channel,))
        writer.start()
            
        try:
            while True:
                d = sys.stdin.read(1)
                if not d:
                    break
                self.handle_input(d)
        except EOFError:
            # user hit ^Z or F6
            pass
        
if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('TARGET')
    args = parser.parse_args()
    username = None
    
    print(args)
    if '@' in args.TARGET:
        username, hostname = args.TARGET.split('@',1)
    else:
        hostname = args.TARGET
        
    port = 22
    if hostname.find(':') >= 0:
        hostname, portstr = hostname.split(':')
        try:
            port = int(portstr)
        except ValueError:
            parser.error("invalid port number %r" % portstr)

    console = RoastedConsole.from_nothing(hostname, port)
    console.login(username=username)