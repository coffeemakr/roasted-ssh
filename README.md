Roasted SSH
===========

Roasted SSH is a SSH client written in python using the [paramiko library](https://github.com/paramiko/paramiko). It allows in-shell makros and special functionallity (e.g. copying a file into a already opened shell).

Usage
-----

**Commands**

To execute a command, simply press <kbd>CTRL X</kbd> and start typing. (The command apears blue while typing if colors are supported.)

<table>
    <tr>
        <td><code>show &lt;what&gt;</code></td><td>Display some information.</td>
    </tr><tr>
        <td><code>quit</code></td><td>Exit without regrets<./td>
    </tr>
</table>
